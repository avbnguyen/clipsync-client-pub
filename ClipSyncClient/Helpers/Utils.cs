﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClipSyncClient
{
    public static class Utils
    {
        private const string KEY = "nva@clipsync#2021";
        
        public static void ShowError(IWin32Window owner, string message, string title)
        {
            MessageBox.Show(owner, message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowError(IWin32Window owner, string message)
        {
            MessageBox.Show(owner, message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowInfo(IWin32Window owner, string message, string title)
        {
            MessageBox.Show(owner, message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowInfo(IWin32Window owner, string message)
        {
            MessageBox.Show(owner, message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult ShowConfirm(IWin32Window owner, string message, string title)
        {
            return MessageBox.Show(owner, message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static DialogResult ShowConfirm(IWin32Window owner, string message)
        {
            return MessageBox.Show(owner, message, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static void ShowWarning(IWin32Window owner, string message, string title)
        {
            MessageBox.Show(owner, message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void ShowWarning(IWin32Window owner, string message)
        {
            MessageBox.Show(owner, message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static string GetMD5FromImage(Image image)
        {
            byte[] bytes = null;

            using (var ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Png);

                bytes = ms.ToArray();
            }

            var md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(bytes);
            var sb = new StringBuilder();

            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2").ToLower());
            }

            return sb.ToString();
        }
    }
}
