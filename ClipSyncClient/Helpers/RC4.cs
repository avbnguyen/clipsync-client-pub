﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClipSyncClient
{
    class RC4
    {
        private const string KEY = "*!&@^#^*&%$@*&#^$*@#^$*&^#%$@#&$(@#*^$(!*^#!(#$&#($";

        public static string Encrypt(string data)
        {
            var unicode = Encoding.UTF8;

            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(KEY), unicode.GetBytes(data)));
        }

        public static byte[] Encrypt(byte[] data)
        {
            return Encrypt(Encoding.UTF8.GetBytes(KEY), data);
        }

        public static byte[] Decrypt(byte[] data)
        {
            return Encrypt(Encoding.UTF8.GetBytes(KEY), data);
        }

        public static string Decrypt(string data)
        {
            var unicode = Encoding.UTF8;

            return unicode.GetString(Encrypt(unicode.GetBytes(KEY), Convert.FromBase64String(data)));
        }

        public static byte[] Encrypt(byte[] key, byte[] data)
        {
            return EncryptOutput(key, data).ToArray();
        }

        public static byte[] Decrypt(byte[] key, byte[] data)
        {
            return EncryptOutput(key, data).ToArray();
        }

        private static byte[] EncryptInitalize(byte[] key)
        {
            var s = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();

            for (int i = 0, j = 0; i < 256; i++)
            {
                j = (j + key[i % key.Length] + s[i]) & 255;

                Swap(s, i, j);
            }

            return s;
        }

        private static IEnumerable<byte> EncryptOutput(byte[] key, IEnumerable<byte> data)
        {
            var s = EncryptInitalize(key);
            var i = 0;
            var j = 0;

            return data.Select((b) =>
            {
                i = (i + 1) & 255;
                j = (j + s[i]) & 255;

                Swap(s, i, j);

                return (byte)(b ^ s[(s[i] + s[j]) & 255]);
            });
        }

        private static void Swap(byte[] s, int i, int j)
        {
            var c = s[i];

            s[i] = s[j];
            s[j] = c;
        }
    }
}
