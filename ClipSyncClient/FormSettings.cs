﻿using ClipSyncClient.Properties;
using Microsoft.Win32;
using Newtonsoft.Json;
using SocketIOClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClipSyncClient
{
    public partial class FormSettings : Form
    {
        #region Variables

        private SocketIO _io;
        private FormMain _formMain;

        #endregion Variables

        #region Initialize

        public FormSettings()
        {
            InitializeComponent();
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        private void Initialize()
        {
            InitializeVariables();
            InitializeEvents();
            InitializeSettings();
        }

        private void InitializeVariables()
        {
            _formMain = Owner as FormMain;
        }

        private void InitializeSettings()
        {
            LoadKeys();
            LoadSettings();
        }

        private void InitializeEvents()
        {

        }

        #endregion Initialize

        #region Form Event Handlers

        private void checkBoxAutoSync_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            SaveSettings();

            DialogResult = DialogResult.OK;

            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            LoadSettings();

            DialogResult = DialogResult.Cancel;

            Close();
        }

        private async void buttonCheckChannel_Click(object sender, EventArgs e)
        {

        }

        private async void buttonTestEndpoint_Click(object sender, EventArgs e)
        {
            try
            {
                var endpoint = textBoxEndpoint.Text.Trim();
                var channel = textBoxChannel.Text.Trim();

                if (endpoint.Length == 0)
                {
                    Utils.ShowWarning(this, string.Format(Resources.ErrorRequiredField, "Endpoint"));
                    textBoxEndpoint.Focus();
                    return;
                }

                if (channel.Length == 0)
                {
                    Utils.ShowWarning(this, string.Format(Resources.ErrorRequiredField, "Channel"));
                    textBoxChannel.Focus();
                    return;
                }

                if (_io == null)
                {
                    _io = new SocketIO($"{endpoint}/{channel}");

                    _io.OnConnected += (s, a) => { Invoke((MethodInvoker)(() => Utils.ShowInfo(this, Resources.TestEndpointOK))); };
                    _io.OnError += (s, a) => { Invoke((MethodInvoker)(() => Utils.ShowError(this, a.ToString()))); };
                }
                else
                {
                    await _io.DisconnectAsync();

                    _io.ServerUri = new Uri($"{endpoint}/{channel}");
                }

                await _io.ConnectAsync();
            }
            catch (Exception ex)
            {
                Utils.ShowError(this, ex.ToString());
            }
        }

        private void textBoxEndpoint_TextChanged(object sender, EventArgs e)
        {
            checkBoxAutoSync.Enabled = textBoxEndpoint.Text.Trim().Length > 0 && textBoxChannel.Text.Trim().Length > 0;
        }

        private void textBoxChannel_TextChanged(object sender, EventArgs e)
        {
            checkBoxAutoSync.Enabled = textBoxEndpoint.Text.Trim().Length > 0 && textBoxChannel.Text.Trim().Length > 0;
        }

        #endregion Form Event Handlers

        #region Methods

        private void LoadSettings()
        {
            textBoxEndpoint.Text = Settings.Default.EndpointURL;
            textBoxChannel.Text = Settings.Default.ChannelId;
            checkBoxAutoSync.Checked = textBoxEndpoint.Text.Trim().Length > 0 && textBoxChannel.Text.Trim().Length > 0 && Settings.Default.AutoSync;

            checkBoxShowAppWinKey.Checked = Settings.Default.ShowAppWinKey;
            checkBoxShowAppControlKey.Checked = Settings.Default.ShowAppControlKey;
            checkBoxShowAppShiftKey.Checked = Settings.Default.ShowAppShiftKey;
            checkBoxShowAppAltKey.Checked = Settings.Default.ShowAppAltKey;

            checkBoxText.Checked = Settings.Default.MonitorText;
            checkBoxImage.Checked = Settings.Default.MonitorImage;
            checkBoxFile.Checked = Settings.Default.MonitorFile;

            comboBoxShowAppKey.SelectedValue = Settings.Default.ShowAppKey;
            checkBoxLaunchAtStartup.Checked = Settings.Default.LaunchAtStartup;

            if (!Settings.Default.AllowSync)
            {
                checkBoxAutoSync.Enabled = false;
                return;
            }

            var rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (checkBoxLaunchAtStartup.Checked)
            {
                rk.SetValue(Application.ProductName, Application.ExecutablePath);
            }
            else
            {
                rk.DeleteValue(Application.ProductName, false);
            }

            if (!(Owner is FormMain formMain) || formMain == null) return;

            formMain.UpdateMonitorType(checkBoxText.Checked, checkBoxImage.Checked, checkBoxFile.Checked);
        }

        private void SaveSettings()
        {
            Settings.Default.EndpointURL = textBoxEndpoint.Text.Trim();
            Settings.Default.ChannelId = textBoxChannel.Text.Trim();
            Settings.Default.AutoSync = checkBoxAutoSync.Checked;
            Settings.Default.ShowAppWinKey = checkBoxShowAppWinKey.Checked;
            Settings.Default.ShowAppControlKey = checkBoxShowAppControlKey.Checked;
            Settings.Default.ShowAppShiftKey = checkBoxShowAppShiftKey.Checked;
            Settings.Default.ShowAppAltKey = checkBoxShowAppAltKey.Checked;
            Settings.Default.ShowAppKey = (int)comboBoxShowAppKey.SelectedValue;
            Settings.Default.MonitorText = checkBoxText.Checked;
            Settings.Default.MonitorImage = checkBoxImage.Checked;
            Settings.Default.MonitorFile = checkBoxFile.Checked;
            Settings.Default.LaunchAtStartup = checkBoxLaunchAtStartup.Checked;

            var rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (checkBoxLaunchAtStartup.Checked)
            {
                rk.SetValue(Application.ProductName, Application.ExecutablePath);
            }
            else
            {
                rk.DeleteValue(Application.ProductName, false);
            }

            if (!Settings.Default.AllowSync) Settings.Default.AutoSync = false;

            Settings.Default.Save();

            if (!(Owner is FormMain formMain)) return;

            formMain.UpdateMonitorType(checkBoxText.Checked, checkBoxImage.Checked, checkBoxFile.Checked);
            formMain.Io.ServerUri = new Uri($"{Settings.Default.EndpointURL}/{Settings.Default.ChannelId}");

            var result = formMain.RegisterShowAppHotKey();

            if (result) Utils.ShowInfo(this, Resources.SaveSettingOK);
        }

        private void LoadKeys()
        {
            var list = new List<KeyInfo>();

            var chars = Enum.GetNames(typeof(Keys));
            var keys = Enum.GetValues(typeof(Keys)).Cast<int>().ToArray();

            for (var i = 0; i < chars.Length; i++)
            {
                list.Add(new KeyInfo() { Key = keys[i], Char = chars[i] });
            }

            comboBoxShowAppKey.DataSource = list;
            comboBoxShowAppKey.DisplayMember = "Char";
            comboBoxShowAppKey.ValueMember = "Key";
        }

        #endregion Methods
    }
}
