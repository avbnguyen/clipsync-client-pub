﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClipSyncClient
{
    class KeyInfo
    {
        public int Key { get; set; }
        public string Char { get; set; }
    }
}
