﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClipSyncClient
{
    class SyncInfo
    {
        public string Id { get; set; }
        public List<ClipboardInfo> Data { get; set; }
    }
}
