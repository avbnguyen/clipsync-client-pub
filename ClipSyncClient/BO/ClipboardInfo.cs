﻿using static WK.Libraries.SharpClipboardNS.SharpClipboard;

namespace ClipSyncClient
{
    public class ClipboardInfo
    {
        public string Id { get; set; }
        public ContentTypes Type { get; set; }

        public dynamic Content { get; set; }

        public string Tag { get; set; }

        public string Hash { get; set; }

        public bool IsLocal { get; set; }

        public bool Available { get; set; }

        public string Status { get; set; }
    }
}
