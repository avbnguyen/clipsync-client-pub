﻿using ClipSyncClient.Properties;
using Newtonsoft.Json;
using SocketIOClient;
using SocketIOClient.Transport;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WK.Libraries.SharpClipboardNS;
using static WK.Libraries.SharpClipboardNS.SharpClipboard;

namespace ClipSyncClient
{
    public partial class FormMain : Form, IDisposable
    {
        #region DLL Imports

        [DllImport("Shlwapi.dll", CharSet = CharSet.Auto)]
        public static extern long StrFormatByteSize(long fileSize, [MarshalAs(UnmanagedType.LPTStr)] StringBuilder buffer, int bufferSize);

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        [DllImport("User32.dll")]
        private static extern int SendMessageW(IntPtr hWnd, int uMsg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll")]
        private static extern IntPtr GetFocus();

        [DllImport("user32.dll")]
        static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);

        [DllImport("kernel32.dll")]
        static extern uint GetCurrentThreadId();

        [DllImport("user32.dll")]
        static extern uint GetWindowThreadProcessId(int hWnd, out int ProcessId);

        [DllImport("user32.dll")]
        static extern int GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(int hWnd);

        #endregion

        #region Variables

        private const int HOTKEY_MESSAGE_ID = 0x0312;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATE = 0x0003;

        private enum KeyModifiers : uint
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }

        private KeyModifiers _appKeyModifier;

        private List<ClipboardInfo> _filterdContents;
        private List<ClipboardInfo> _contents;
        private SharpClipboard _clipboard;

        private FormSettings _formSetting;

        private bool _isFiltered;
        private bool _isPasting;

        private string _selectedFolder;

        public SocketIO Io { get; private set; }
        public string ClientId { get; private set; }

        #endregion

        #region Overriding

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case HOTKEY_MESSAGE_ID:
                    {
                        var key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                        var modifier = (KeyModifiers)((int)m.LParam & 0xFFFF);

                        if (modifier == _appKeyModifier && (int)key == Settings.Default.ShowAppKey) ShowMainApp();

                        break;
                    }
                case WM_MOUSEACTIVATE:
                    {
                        m.Result = (IntPtr)MA_NOACTIVATE;
                        break;
                    }
                default:
                    {
                        base.WndProc(ref m);
                        break;
                    }
            }
        }

        #endregion

        #region Initialize

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        private void Initialize()
        {
            InitializeVariables();
            InitializeEvents();
            InitializeSettings();
        }

        private void InitializeVariables()
        {
            _contents = new List<ClipboardInfo>();
            _filterdContents = new List<ClipboardInfo>();

            _formSetting = new FormSettings();

            ClientId = Guid.NewGuid().ToString();
        }

        private void InitializeSettings()
        {
            ShowSettingForm();
            RegisterHotKeys();
            Connect();

            comboBoxFileType.SelectedIndex = Settings.Default.FileType;
        }

        private void InitializeEvents()
        {

        }

        #endregion Initialize

        #region Form Event Handlers

        private void FormMain_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();

                notifyIconTray.Visible = true;
            }
        }

        private async void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_clipboard != null) _clipboard.StopMonitoring();

            if (notifyIconTray != null) notifyIconTray.Visible = false;

            if (Io != null) await Io.DisconnectAsync();
        }

        private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopySelected();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteSelected();
        }

        private void locateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView.SelectedIndices.Count == 0) return;

            var path = _contents[listView.SelectedIndices[0]].Tag;

            if (!File.Exists(path)) return;

            Process.Start("explorer.exe", "/select, " + path);
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PasteSelectedContent();
        }

        private void comboBoxFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var idx = comboBoxFileType.SelectedIndex;

            _isFiltered = idx != 0;

            UpdateList();
        }

        private void buttonClearAll_Click(object sender, EventArgs e)
        {
            if (Utils.ShowConfirm(this, Resources.AreYouSure, Resources.ClearAll) == DialogResult.Cancel) return;

            _contents.Clear();
            _filterdContents.Clear();

            UpdateList();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            if (_formSetting.Visible) return;

            _formSetting.ShowDialog(this);
        }

        private void listView_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            var idx = e.ItemIndex;
            var list = _isFiltered ? _filterdContents : _contents;

            var content = list[idx].Type == ContentTypes.Image ? list[idx].Tag : (string)list[idx].Content;

            e.Item = new ListViewItem(new string[] { content });
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            PasteSelectedContent();
        }

        private void listView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.A:
                    {
                        if (e.Modifiers == Keys.Control)
                        {
                            int count = listView.SelectedIndices.Count;

                            listView.SelectedIndices.Clear();

                            if (count != _contents.Count)
                            {
                                Enumerable.Range(0, _contents.Count).ToList().ForEach(x =>
                                {
                                    listView.SelectedIndices.Add(x);
                                });
                            }
                        }

                        break;
                    }
                case Keys.Delete:
                    {
                        DeleteSelected();
                        break;
                    }
                case Keys.C:
                    {
                        if (e.Modifiers != Keys.Control) return;

                        CopySelected();
                        break;
                    }
                case Keys.R:
                    {
                        if (e.Modifiers != Keys.Control) return;

                        ReceiveFile();
                        break;
                    }
            }
        }

        private void receiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReceiveFile();
        }

        private void notifyIconTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowMainApp();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_formSetting.Visible) return;

            _formSetting.ShowDialog(this);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowMainApp();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utils.ShowInfo(this, Resources.About);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIconTray.Visible = false;

            Environment.Exit(0);
        }

        private void contextMenuStripList_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var selectedIndices = listView.SelectedIndices.Cast<int>().ToList();

            if (selectedIndices.Count == 0)
            {
                e.Cancel = true;
                return;
            }

            var idx = selectedIndices[0];

            viewToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].Type == ContentTypes.Image;
            locateToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].Type == ContentTypes.Files;

            receiveToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].Type == ContentTypes.Files && !_contents[idx].IsLocal;
            copyToClipboardToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].IsLocal;
            viewToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].IsLocal;
            locateToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].IsLocal && _contents[idx].Type == ContentTypes.Files;
            pasteToolStripMenuItem.Visible = selectedIndices.Count == 1 && _contents[idx].IsLocal;
        }

        #endregion Form Event Handlers

        #region Methods

        private async void ReceiveFile()
        {
            if (listView.SelectedIndices.Count == 0 || Io == null || Io.Disconnected) return;

            var indices = listView.SelectedIndices.Cast<int>().OrderBy(x => x).ToList();

            if (indices.Count > 1)
            {
                Utils.ShowWarning(this, Resources.ErrorAllowSingleFileOnly);
                return;
            }

            var item = _contents[indices[0]];

            if (item.IsLocal || item.Type != ContentTypes.Files) return;

            using (var fbd = new FolderBrowserDialog())
            {
                fbd.Description = Resources.ChooseFolderForReceiveFile;
                fbd.ShowNewFolderButton = true;

                if (fbd.ShowDialog() != DialogResult.OK || string.IsNullOrEmpty(fbd.SelectedPath)) return;

                _selectedFolder = fbd.SelectedPath;

                ToggleForm(false);
                Cursor.Current = Cursors.WaitCursor;

                await Io.EmitAsync("receive-file", item.Id);
            }
        }

        private async void Connect()
        {
            if (Settings.Default.EndpointURL.Length == 0 || Settings.Default.ChannelId.Length == 0) return;

            Io = new SocketIO($"{Settings.Default.EndpointURL}/{Settings.Default.ChannelId}");

            Io.OnConnected += (s, e) =>
            {
                Io.On("sync", OnSync);
                Io.On("receive-file", OnReceiveFile);
                Io.On("file-received", OnFileReceived);

                Invoke((MethodInvoker)(() => SetupClipboard()));
            };

            Io.OnDisconnected += (s, e) =>
            {
                Io.Off("sync");
                Io.Off("file-received");

                ToggleForm(true);
                Cursor.Current = Cursors.Default;
            };

            await Io.ConnectAsync();
        }

        private void OnFileReceived(SocketIOResponse r)
        {
            Invoke((MethodInvoker)(() =>
            {
                try
                {
                    if (r == null || (r.GetValue<BO.FileInfo>() == null) || string.IsNullOrEmpty(_selectedFolder)) return;

                    var fileInfo = r.GetValue<BO.FileInfo>();
                    var filePath = Path.Combine(_selectedFolder, fileInfo.FileName);

                    File.WriteAllBytes(filePath, fileInfo.Data);

                    var idx = listView.SelectedIndices[0];

                    _contents[idx].IsLocal = true;
                    _contents[idx].Tag = filePath;

                    ToggleForm(true);

                    Cursor.Current = Cursors.Default;

                    Utils.ShowInfo(this, Resources.FileSyncedOK);

                }
                catch (Exception ex)
                {
                    Utils.ShowError(this, ex.ToString());
                }
            }));
        }

        private async void OnReceiveFile(SocketIOResponse r)
        {
            var id = r == null ? "" : r.GetValue<string>();

            if (string.IsNullOrEmpty(id)) return;

            var item = _contents.FirstOrDefault(x => x.Id.Equals(id));

            if (item == null || item.Type != ContentTypes.Files) return;

            await Io.EmitAsync("file-received", new BO.FileInfo() { FileName = Path.GetFileName(item.Tag), Data = File.ReadAllBytes(item.Tag) });
        }

        private void OnSync(SocketIOResponse r)
        {
            try
            {
                var content = r.GetValue<string>();
                var decrypted = RC4.Decrypt(content);

                var syncInfo = JsonConvert.DeserializeObject<SyncInfo>(decrypted);

                if (syncInfo == null || syncInfo.Id == ClientId) return;

                _contents.AddRange(syncInfo.Data);

                Invoke((MethodInvoker)(() => UpdateList()));
            }
            catch (Exception ex)
            {

            }
        }

        private async void Sync(List<ClipboardInfo> items)
        {
            if (!Settings.Default.AllowSync || Io == null || Io.Disconnected || items.Count == 0) return;

            var syncItems = items.Select(x => new ClipboardInfo() { Available = false, Content = x.Content, Hash = x.Hash, Id = x.Id, IsLocal = false, Tag = x.Tag, Type = x.Type }).ToList();

            var syncInfo = new SyncInfo() { Id = ClientId, Data = syncItems };
            var content = JsonConvert.SerializeObject(syncInfo);
            var encrypted = RC4.Encrypt(content);

            await Io.EmitAsync("sync", encrypted);
        }

        private string ToPrettySize(long fileSize)
        {
            var sb = new StringBuilder(11);

            StrFormatByteSize(fileSize, sb, sb.Capacity);

            return sb.ToString();
        }

        private void SetupClipboard()
        {
            _clipboard = new SharpClipboard();

            _clipboard.ClipboardChanged += (s, e) =>
            {
                if (_isPasting) return;

                var newItems = new List<ClipboardInfo>();

                switch (e.ContentType)
                {
                    case ContentTypes.Text:
                        {
                            var content = _clipboard.ClipboardText.Trim();

                            if (content == null || string.IsNullOrEmpty(content) || _contents.Any(x => x.Content is string && ((string)x.Content).Equals(content))) return;

                            newItems.Add(new ClipboardInfo() { Id = Guid.NewGuid().ToString(), Content = content, Type = ContentTypes.Text, IsLocal = true, Available = true });
                            break;
                        }
                    case ContentTypes.Image:
                        {
                            using (var ms = new MemoryStream())
                            {
                                _clipboard.ClipboardImage.Save(ms, ImageFormat.Png);

                                var img = _clipboard.ClipboardImage;
                                var content = Convert.ToBase64String(ms.ToArray());

                                if (content == null) return;

                                var md5Hash = Utils.GetMD5FromImage(img);

                                if (_contents.Any(x => x.Hash != null && x.Hash.Equals(md5Hash))) return;

                                newItems.Add(new ClipboardInfo() { Id = Guid.NewGuid().ToString(), Content = content, Type = ContentTypes.Image, Hash = md5Hash, Tag = string.Format(Resources.ImageContentFormat, md5Hash, img.Size.Width, img.Size.Height), IsLocal = true, Available = true });
                            }
                            break;
                        }
                    case ContentTypes.Files:
                        {
                            var content = _clipboard.ClipboardFiles;

                            if (content == null || content.Count == 0) return;

                            content.ForEach(x =>
                            {
                                if (!File.Exists(x) || new FileInfo(x).Length > Settings.Default.FileSizeLimit || _contents.Any(i => i.Tag != null && i.Tag.Equals(x))) return;

                                var attr = File.GetAttributes(x);

                                var c = attr.HasFlag(FileAttributes.Directory) ? string.Format(Resources.FolderContentFormat, x) : string.Format(Resources.FileContentFormat, x, ToPrettySize(new FileInfo(x).Length));

                                newItems.Add(new ClipboardInfo() { Id = Guid.NewGuid().ToString(), Content = c, Type = ContentTypes.Files, Tag = x, IsLocal = true, Available = true });
                            });
                            break;
                        }
                }

                if (newItems.Count == 0) return;

                _contents.AddRange(newItems);

                UpdateList();
                Sync(newItems);
            };

            _clipboard.ObservableFormats.Texts = Settings.Default.MonitorText;
            _clipboard.ObservableFormats.Images = Settings.Default.MonitorImage;
            _clipboard.ObservableFormats.Files = Settings.Default.MonitorFile;

            _clipboard.StartMonitoring();
        }

        private void CopySelected()
        {
            if (listView.SelectedIndices.Count == 0) return;

            var selectedIndices = listView.SelectedIndices.Cast<int>().OrderBy(x => x).ToList();
            var contentList = _isFiltered ? _filterdContents : _contents;
            var selectedTypes = contentList.Where((x, i) => selectedIndices.Contains(i)).GroupBy(x => x.Type).Select(x => x.Key).ToList();

            if (selectedTypes == null || selectedTypes.Count == 0) return;

            if (selectedTypes.Count() > 1)
            {
                Utils.ShowWarning(this, Resources.ErrorMultipleTypeSelected);
            }
            else
            {
                var type = selectedTypes.FirstOrDefault();

                switch (type)
                {
                    case ContentTypes.Text:
                        {
                            var list = new List<string>();

                            selectedIndices.ForEach(x =>
                            {
                                list.Add((string)contentList[x].Content);
                            });

                            Clipboard.SetText(string.Join(Environment.NewLine, list));
                            break;
                        }
                    case ContentTypes.Image:
                        {
                            if (selectedIndices.Count > 1)
                            {
                                Utils.ShowWarning(this, string.Format(Resources.ErrorCopyMultipleItems, "image"));
                            }
                            else
                            {
                                var idx = selectedIndices.FirstOrDefault();

                                var content = (string)contentList[idx].Content;
                                var bytes = Convert.FromBase64String(content);

                                using (var ms = new MemoryStream(bytes))
                                {
                                    Clipboard.SetImage(Image.FromStream(ms));
                                }
                            }
                            break;
                        }
                    case ContentTypes.Files:
                        {
                            var paths = new StringCollection();

                            selectedIndices.ForEach(x =>
                            {
                                paths.Add(contentList[x].Tag);
                            });

                            if (paths.Count == 0) return;

                            Clipboard.SetFileDropList(paths);
                            break;
                        }
                }
            }

            Visible = false;
        }

        private void ShowMainApp()
        {
            Show();

            WindowState = FormWindowState.Normal;

            notifyIconTray.Visible = false;
        }

        private void RegisterHotKeys()
        {
            RegisterShowAppHotKey();
        }

        private void ShowSettingForm()
        {
            if (Settings.Default.EndpointURL.Length == 0 || Settings.Default.ChannelId.Length == 0)
            {
                var formSetting = new FormSettings();

                formSetting.ShowDialog(this);
            }
        }

        public bool RegisterShowAppHotKey()
        {
            var appModifierKey = KeyModifiers.None;
            var result = false;

            if (Settings.Default.ShowAppWinKey) appModifierKey |= KeyModifiers.WinKey;
            if (Settings.Default.ShowAppControlKey) appModifierKey |= KeyModifiers.Control;
            if (Settings.Default.ShowAppShiftKey) appModifierKey |= KeyModifiers.Shift;
            if (Settings.Default.ShowAppAltKey) appModifierKey |= KeyModifiers.Alt;

            if (appModifierKey != KeyModifiers.None)
            {
                _appKeyModifier = appModifierKey;

                UnregisterHotKey(Handle, Settings.Default.ShowAppHotkeyId);

                result = RegisterHotKey(Handle, Settings.Default.ShowAppHotkeyId, (int)appModifierKey, Settings.Default.ShowAppKey);

                if (!result) Utils.ShowWarning(this, string.Format(Resources.ErrorRegisterHotKey, "Show app"));
            }

            return result;
        }

        private void DeleteSelected()
        {
            listView.SelectedIndices.Cast<int>().OrderByDescending(x => x).ToList().ForEach(x =>
            {
                _contents.RemoveAt(x);
            });

            UpdateList();

            listView.SelectedIndices.Clear();
        }

        private void UpdateList()
        {
            if (_isFiltered)
            {
                var idx = comboBoxFileType.SelectedIndex;

                _filterdContents.Clear();

                switch (idx)
                {
                    case 1:
                        {
                            _filterdContents.AddRange(_contents.Where(x => x.Type == ContentTypes.Text).ToList());
                            break;
                        }
                    case 2:
                        {
                            _filterdContents.AddRange(_contents.Where(x => x.Type == ContentTypes.Files).ToList());
                            break;
                        }
                    case 3:
                        {
                            _filterdContents.AddRange(_contents.Where(x => x.Type == ContentTypes.Image).ToList());
                            break;
                        }
                }
            }

            listView.BeginUpdate();
            listView.VirtualListSize = _isFiltered ? _filterdContents.Count : _contents.Count;
            listView.EndUpdate();
        }

        private void PasteSelectedContent()
        {
            if (listView.SelectedIndices.Count == 0) return;

            var selectedIndices = listView.SelectedIndices.Cast<int>().OrderBy(x => x).ToList();
            var contentList = _isFiltered ? _filterdContents : _contents;

            var selectedTypes = contentList.Where((x, i) => selectedIndices.Contains(i)).GroupBy(x => x.Type).Select(x => x.Key).ToList();

            if (selectedTypes == null || selectedTypes.Count == 0) return;

            if (selectedTypes.Count() > 1)
            {
                Utils.ShowWarning(this, Resources.ErrorMultipleTypeSelected);
            }
            else
            {
                var type = selectedTypes.FirstOrDefault();
                var current = Clipboard.GetDataObject();

                Visible = false;

                _isPasting = true;

                switch (type)
                {
                    case ContentTypes.Text:
                        {
                            var list = new List<string>();

                            selectedIndices.ForEach(x =>
                            {
                                list.Add((string)contentList[x].Content);
                            });

                            Clipboard.SetText(string.Join(Environment.NewLine, list));

                            Thread.Sleep(Settings.Default.TextDelay);

                            SendKeys.Send("^{v}");
                            break;
                        }
                    case ContentTypes.Image:
                        {
                            if (selectedIndices.Count > 1)
                            {
                                Utils.ShowWarning(this, string.Format(Resources.ErrorCopyMultipleItems, "image"));
                            }
                            else
                            {
                                var idx = selectedIndices.FirstOrDefault();
                                var handle = GetForegroundWindow();

                                GetWindowThreadProcessId(handle, out int pid);

                                var process = Process.GetProcessById(pid);
                                var content = (string)contentList[idx].Content;
                                var bytes = Convert.FromBase64String(content);
                                var ms = new MemoryStream(bytes);
                                var image = Image.FromStream(ms);

                                if (process.ProcessName == "explorer")
                                {
                                    var file = Path.GetFileName(Path.GetTempFileName().Replace(".tmp", ".png"));

                                    image.Save(file, ImageFormat.Png);

                                    var paths = new StringCollection
                                    {
                                        Path.Combine(Application.StartupPath, file)
                                    };

                                    if (paths.Count == 0)
                                    {
                                        _isPasting = false;

                                        Clipboard.SetDataObject(current);
                                        return;
                                    }

                                    Clipboard.SetFileDropList(paths);

                                    Thread.Sleep(Settings.Default.ImageDelay);

                                    SendKeys.Send("^{v}");
                                }
                                else
                                {
                                    Clipboard.SetDataObject(image);

                                    Thread.Sleep(Settings.Default.ImageDelay);

                                    SendKeys.Send("^{v}");
                                }

                                ms = null;
                                image = null;
                                bytes = null;
                            }
                            break;
                        }
                    case ContentTypes.Files:
                        {
                            var paths = new StringCollection();

                            selectedIndices.ForEach(x =>
                            {
                                paths.Add(contentList[x].Tag);
                            });

                            if (paths.Count == 0)
                            {
                                _isPasting = false;

                                Clipboard.SetDataObject(current);
                                return;
                            }

                            Clipboard.SetFileDropList(paths);

                            Thread.Sleep(Settings.Default.FileDelay);

                            SendKeys.Send("^{v}");
                            break;
                        }
                }

                Clipboard.SetDataObject(current);

                _isPasting = false;
            }
        }

        public void UpdateMonitorType(bool text, bool image, bool file)
        {
            if (_clipboard == null) return;

            _clipboard.ObservableFormats.Texts = text;
            _clipboard.ObservableFormats.Images = image;
            _clipboard.ObservableFormats.Files = file;
        }

        private void ToggleForm(bool status)
        {
            buttonClearAll.Enabled = status;
            buttonSettings.Enabled = status;
            listView.Enabled = status;
            comboBoxFileType.Enabled = status;
        }

        #endregion
    }
}
