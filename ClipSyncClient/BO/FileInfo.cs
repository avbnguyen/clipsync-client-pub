﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClipSyncClient.BO
{
    class FileInfo
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
    }
}
