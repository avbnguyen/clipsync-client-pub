﻿
namespace ClipSyncClient
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this.toolTipChannel = new System.Windows.Forms.ToolTip(this.components);
            this.buttonChannelTooltips = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.comboBoxShowAppKey = new System.Windows.Forms.ComboBox();
            this.checkBoxShowAppAltKey = new System.Windows.Forms.CheckBox();
            this.checkBoxShowAppShiftKey = new System.Windows.Forms.CheckBox();
            this.checkBoxShowAppControlKey = new System.Windows.Forms.CheckBox();
            this.checkBoxShowAppWinKey = new System.Windows.Forms.CheckBox();
            this.labelShowApp = new System.Windows.Forms.Label();
            this.buttonTestEndpoint = new System.Windows.Forms.Button();
            this.checkBoxAutoSync = new System.Windows.Forms.CheckBox();
            this.textBoxChannel = new System.Windows.Forms.TextBox();
            this.labelChannel = new System.Windows.Forms.Label();
            this.textBoxEndpoint = new System.Windows.Forms.TextBox();
            this.labelEndpoint = new System.Windows.Forms.Label();
            this.labelMonitor = new System.Windows.Forms.Label();
            this.checkBoxText = new System.Windows.Forms.CheckBox();
            this.checkBoxImage = new System.Windows.Forms.CheckBox();
            this.checkBoxFile = new System.Windows.Forms.CheckBox();
            this.checkBoxLaunchAtStartup = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // toolTipChannel
            // 
            this.toolTipChannel.ToolTipTitle = "Channel";
            // 
            // buttonChannelTooltips
            // 
            this.buttonChannelTooltips.Location = new System.Drawing.Point(399, 52);
            this.buttonChannelTooltips.Name = "buttonChannelTooltips";
            this.buttonChannelTooltips.Size = new System.Drawing.Size(27, 20);
            this.buttonChannelTooltips.TabIndex = 18;
            this.buttonChannelTooltips.Text = "?";
            this.toolTipChannel.SetToolTip(this.buttonChannelTooltips, "Set your channel to sync content only with your clients");
            this.buttonChannelTooltips.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(318, 225);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(399, 225);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 2;
            this.buttonApply.Text = "&Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // comboBoxShowAppKey
            // 
            this.comboBoxShowAppKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShowAppKey.FormattingEnabled = true;
            this.comboBoxShowAppKey.Location = new System.Drawing.Point(286, 107);
            this.comboBoxShowAppKey.Name = "comboBoxShowAppKey";
            this.comboBoxShowAppKey.Size = new System.Drawing.Size(109, 21);
            this.comboBoxShowAppKey.TabIndex = 25;
            // 
            // checkBoxShowAppAltKey
            // 
            this.checkBoxShowAppAltKey.AutoSize = true;
            this.checkBoxShowAppAltKey.Location = new System.Drawing.Point(242, 109);
            this.checkBoxShowAppAltKey.Name = "checkBoxShowAppAltKey";
            this.checkBoxShowAppAltKey.Size = new System.Drawing.Size(38, 17);
            this.checkBoxShowAppAltKey.TabIndex = 24;
            this.checkBoxShowAppAltKey.Text = "Alt";
            this.checkBoxShowAppAltKey.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowAppShiftKey
            // 
            this.checkBoxShowAppShiftKey.AutoSize = true;
            this.checkBoxShowAppShiftKey.Location = new System.Drawing.Point(189, 109);
            this.checkBoxShowAppShiftKey.Name = "checkBoxShowAppShiftKey";
            this.checkBoxShowAppShiftKey.Size = new System.Drawing.Size(47, 17);
            this.checkBoxShowAppShiftKey.TabIndex = 23;
            this.checkBoxShowAppShiftKey.Text = "Shift";
            this.checkBoxShowAppShiftKey.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowAppControlKey
            // 
            this.checkBoxShowAppControlKey.AutoSize = true;
            this.checkBoxShowAppControlKey.Location = new System.Drawing.Point(124, 109);
            this.checkBoxShowAppControlKey.Name = "checkBoxShowAppControlKey";
            this.checkBoxShowAppControlKey.Size = new System.Drawing.Size(59, 17);
            this.checkBoxShowAppControlKey.TabIndex = 22;
            this.checkBoxShowAppControlKey.Text = "Control";
            this.checkBoxShowAppControlKey.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowAppWinKey
            // 
            this.checkBoxShowAppWinKey.AutoSize = true;
            this.checkBoxShowAppWinKey.Location = new System.Drawing.Point(73, 109);
            this.checkBoxShowAppWinKey.Name = "checkBoxShowAppWinKey";
            this.checkBoxShowAppWinKey.Size = new System.Drawing.Size(45, 17);
            this.checkBoxShowAppWinKey.TabIndex = 21;
            this.checkBoxShowAppWinKey.Text = "Win";
            this.checkBoxShowAppWinKey.UseVisualStyleBackColor = true;
            // 
            // labelShowApp
            // 
            this.labelShowApp.AutoSize = true;
            this.labelShowApp.Location = new System.Drawing.Point(12, 110);
            this.labelShowApp.Name = "labelShowApp";
            this.labelShowApp.Size = new System.Drawing.Size(55, 13);
            this.labelShowApp.TabIndex = 20;
            this.labelShowApp.Text = "Show &app";
            // 
            // buttonTestEndpoint
            // 
            this.buttonTestEndpoint.Location = new System.Drawing.Point(399, 20);
            this.buttonTestEndpoint.Name = "buttonTestEndpoint";
            this.buttonTestEndpoint.Size = new System.Drawing.Size(75, 23);
            this.buttonTestEndpoint.TabIndex = 15;
            this.buttonTestEndpoint.Text = "&Test";
            this.buttonTestEndpoint.UseVisualStyleBackColor = true;
            this.buttonTestEndpoint.Click += new System.EventHandler(this.buttonTestEndpoint_Click);
            // 
            // checkBoxAutoSync
            // 
            this.checkBoxAutoSync.AutoSize = true;
            this.checkBoxAutoSync.Enabled = false;
            this.checkBoxAutoSync.Location = new System.Drawing.Point(73, 137);
            this.checkBoxAutoSync.Name = "checkBoxAutoSync";
            this.checkBoxAutoSync.Size = new System.Drawing.Size(226, 17);
            this.checkBoxAutoSync.TabIndex = 19;
            this.checkBoxAutoSync.Text = "&Auto sync clipboard content to your clients";
            this.checkBoxAutoSync.UseVisualStyleBackColor = true;
            this.checkBoxAutoSync.CheckedChanged += new System.EventHandler(this.checkBoxAutoSync_CheckedChanged);
            // 
            // textBoxChannel
            // 
            this.textBoxChannel.Location = new System.Drawing.Point(73, 52);
            this.textBoxChannel.Name = "textBoxChannel";
            this.textBoxChannel.Size = new System.Drawing.Size(320, 20);
            this.textBoxChannel.TabIndex = 17;
            this.textBoxChannel.TextChanged += new System.EventHandler(this.textBoxChannel_TextChanged);
            // 
            // labelChannel
            // 
            this.labelChannel.AutoSize = true;
            this.labelChannel.Location = new System.Drawing.Point(21, 55);
            this.labelChannel.Name = "labelChannel";
            this.labelChannel.Size = new System.Drawing.Size(46, 13);
            this.labelChannel.TabIndex = 16;
            this.labelChannel.Text = "&Channel";
            // 
            // textBoxEndpoint
            // 
            this.textBoxEndpoint.Location = new System.Drawing.Point(73, 21);
            this.textBoxEndpoint.Name = "textBoxEndpoint";
            this.textBoxEndpoint.Size = new System.Drawing.Size(322, 20);
            this.textBoxEndpoint.TabIndex = 14;
            this.textBoxEndpoint.TextChanged += new System.EventHandler(this.textBoxEndpoint_TextChanged);
            // 
            // labelEndpoint
            // 
            this.labelEndpoint.AutoSize = true;
            this.labelEndpoint.Location = new System.Drawing.Point(18, 24);
            this.labelEndpoint.Name = "labelEndpoint";
            this.labelEndpoint.Size = new System.Drawing.Size(49, 13);
            this.labelEndpoint.TabIndex = 13;
            this.labelEndpoint.Text = "&Endpoint";
            // 
            // labelMonitor
            // 
            this.labelMonitor.AutoSize = true;
            this.labelMonitor.Location = new System.Drawing.Point(25, 83);
            this.labelMonitor.Name = "labelMonitor";
            this.labelMonitor.Size = new System.Drawing.Size(42, 13);
            this.labelMonitor.TabIndex = 28;
            this.labelMonitor.Text = "&Monitor";
            // 
            // checkBoxText
            // 
            this.checkBoxText.AutoSize = true;
            this.checkBoxText.Checked = true;
            this.checkBoxText.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxText.Location = new System.Drawing.Point(73, 83);
            this.checkBoxText.Name = "checkBoxText";
            this.checkBoxText.Size = new System.Drawing.Size(47, 17);
            this.checkBoxText.TabIndex = 29;
            this.checkBoxText.Text = "&Text";
            this.checkBoxText.UseVisualStyleBackColor = true;
            // 
            // checkBoxImage
            // 
            this.checkBoxImage.AutoSize = true;
            this.checkBoxImage.Checked = true;
            this.checkBoxImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxImage.Location = new System.Drawing.Point(129, 83);
            this.checkBoxImage.Name = "checkBoxImage";
            this.checkBoxImage.Size = new System.Drawing.Size(55, 17);
            this.checkBoxImage.TabIndex = 29;
            this.checkBoxImage.Text = "&Image";
            this.checkBoxImage.UseVisualStyleBackColor = true;
            // 
            // checkBoxFile
            // 
            this.checkBoxFile.AutoSize = true;
            this.checkBoxFile.Checked = true;
            this.checkBoxFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFile.Location = new System.Drawing.Point(192, 83);
            this.checkBoxFile.Name = "checkBoxFile";
            this.checkBoxFile.Size = new System.Drawing.Size(42, 17);
            this.checkBoxFile.TabIndex = 29;
            this.checkBoxFile.Text = "&File";
            this.checkBoxFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxLaunchAtStartup
            // 
            this.checkBoxLaunchAtStartup.AutoSize = true;
            this.checkBoxLaunchAtStartup.Location = new System.Drawing.Point(73, 165);
            this.checkBoxLaunchAtStartup.Name = "checkBoxLaunchAtStartup";
            this.checkBoxLaunchAtStartup.Size = new System.Drawing.Size(109, 17);
            this.checkBoxLaunchAtStartup.TabIndex = 30;
            this.checkBoxLaunchAtStartup.Text = "&Launch at startup";
            this.checkBoxLaunchAtStartup.UseVisualStyleBackColor = true;
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(488, 260);
            this.Controls.Add(this.checkBoxLaunchAtStartup);
            this.Controls.Add(this.checkBoxFile);
            this.Controls.Add(this.checkBoxImage);
            this.Controls.Add(this.checkBoxText);
            this.Controls.Add(this.labelMonitor);
            this.Controls.Add(this.comboBoxShowAppKey);
            this.Controls.Add(this.checkBoxShowAppAltKey);
            this.Controls.Add(this.checkBoxShowAppShiftKey);
            this.Controls.Add(this.checkBoxShowAppControlKey);
            this.Controls.Add(this.checkBoxShowAppWinKey);
            this.Controls.Add(this.labelShowApp);
            this.Controls.Add(this.buttonChannelTooltips);
            this.Controls.Add(this.buttonTestEndpoint);
            this.Controls.Add(this.checkBoxAutoSync);
            this.Controls.Add(this.textBoxChannel);
            this.Controls.Add(this.labelChannel);
            this.Controls.Add(this.textBoxEndpoint);
            this.Controls.Add(this.labelEndpoint);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.buttonCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTipChannel;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.ComboBox comboBoxShowAppKey;
        private System.Windows.Forms.CheckBox checkBoxShowAppAltKey;
        private System.Windows.Forms.CheckBox checkBoxShowAppShiftKey;
        private System.Windows.Forms.CheckBox checkBoxShowAppControlKey;
        private System.Windows.Forms.CheckBox checkBoxShowAppWinKey;
        private System.Windows.Forms.Label labelShowApp;
        private System.Windows.Forms.Button buttonChannelTooltips;
        private System.Windows.Forms.Button buttonTestEndpoint;
        private System.Windows.Forms.CheckBox checkBoxAutoSync;
        private System.Windows.Forms.TextBox textBoxChannel;
        private System.Windows.Forms.Label labelChannel;
        private System.Windows.Forms.TextBox textBoxEndpoint;
        private System.Windows.Forms.Label labelEndpoint;
        private System.Windows.Forms.Label labelMonitor;
        private System.Windows.Forms.CheckBox checkBoxText;
        private System.Windows.Forms.CheckBox checkBoxImage;
        private System.Windows.Forms.CheckBox checkBoxFile;
        private System.Windows.Forms.CheckBox checkBoxLaunchAtStartup;
    }
}