﻿using ClipSyncClient.Properties;
using System;
using System.Threading;
using System.Windows.Forms;

namespace ClipSyncClient
{
    static class Program
    {
        private static Mutex _mutex;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            _mutex = new Mutex(true, Application.ProductName, out bool createNew);

            if (!createNew && !Settings.Default.MultipleInstance)
            {
                Utils.ShowWarning(null, Resources.ErrorAppAlreadyRun);
                return;
            }

            var formMain = new FormMain
            {
                WindowState = FormWindowState.Minimized,
            };

            Application.Run(formMain);
        }
    }
}
